package it.corso.jwt;

import java.io.IOException;
import java.security.Key;
import java.util.List;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;


@JWTTokenNeeded
@Provider
public class JWTTOkenNeededFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		Secured annotationRole = resourceInfo.getResourceMethod().getAnnotation(Secured.class);

		if (annotationRole == null) {
			annotationRole = resourceInfo.getResourceClass().getAnnotation(Secured.class);
		}

		String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			throw new NotAuthorizedException("Auth not valid");
		}

		String token = authHeader.substring("Bearer".length()).trim();

		try {
			byte[] secret = "stringachiavesegreta12345123411111".getBytes();
			Key key = Keys.hmacShaKeyFor(secret);

			// prende il token e controlla la firma e fa ritornare i claims (i campi del
			// token)
			Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(secret).build().parseClaimsJws(token);
			Claims bodyClaims = claims.getBody();

			List<String> ruoliToken = bodyClaims.get("ruoli", List.class);

			Boolean hasRole = false;

			for (String ruolo : ruoliToken) {
				if (ruolo.equals(annotationRole.role())) {
					hasRole = true;
				}
			}

			if (!hasRole) {
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
			}

		} catch (Exception e) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}

	}

}