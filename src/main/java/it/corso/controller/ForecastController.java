package it.corso.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import it.corso.dto.ForecastRequestDTO;
import it.corso.jwt.JWTTokenNeeded;
import it.corso.jwt.Secured;
import it.corso.service.ForecastRequestService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/forecast")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ForecastController {

    @Autowired
    private ForecastRequestService forecastRequestService;

    @GET
    @Path("/get")
    public Response createForecast(@QueryParam("city") String city, @QueryParam("email") String email) {
        try {

            if (city == null || city.isEmpty() || email == null || email.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            // coordinate 1 = latitude, coordinate 2 = longitude
            Double[] coordinates = forecastRequestService.getCoordinatesFromCityName(city);

            URI uri = new URI("https://api.open-meteo.com/v1/forecast?latitude=" + coordinates[0] + "&longitude="
                    + coordinates[1] + "&daily=weather_code,temperature_2m_max,temperature_2m_min");

            HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            int responseCode = connection.getResponseCode();

            if (responseCode != 200) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String inputLine;

            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject responseJson = new JSONObject(response.toString());

            ForecastRequestDTO forecastRequestDTO = forecastRequestService.createForecast(responseJson, email, city);

            return Response.ok().entity(forecastRequestDTO).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/search")
    public Response searchForecast(@QueryParam("city") String city) {
        try {
            if (city == null || city.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            // coordinate 1 = latitude, coordinate 2 = longitude
            Double[] coordinates = forecastRequestService.getCoordinatesFromCityName(city);

            URI uri = new URI("https://api.open-meteo.com/v1/forecast?latitude=" + coordinates[0] + "&longitude="
                    + coordinates[1] + "&daily=weather_code,temperature_2m_max,temperature_2m_min");

            HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            int responseCode = connection.getResponseCode();

            if (responseCode != 200) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String inputLine;

            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject responseJson = new JSONObject(response.toString());

            ForecastRequestDTO forecastRequestDTO = forecastRequestService.searchForecast(responseJson, city);

            return Response.ok().entity(forecastRequestDTO).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/get/all")
    public Response getAllForecastRequestFromEmail(@QueryParam("email") String email) {
        try {
            if (email == null || email.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            return Response.ok().entity(forecastRequestService.getAllForecastRequestFromEmail(email)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

}
