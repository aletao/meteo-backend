package it.corso.controller;

import java.security.Key;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.corso.dto.UserDTO;
import it.corso.dto.UserLoginDTO;
import it.corso.dto.UserLoginResponseDTO;
import it.corso.dto.UserRegistrationDTO;
import it.corso.model.User;
import it.corso.service.UserService;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    @Autowired
    private UserService userService;

    private ModelMapper mapper = new ModelMapper();

    @POST
    @Path("/register")
    public Response userRegistration(@Valid @RequestBody UserRegistrationDTO userRegistrationDTO) {
        try {
            if (Pattern.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}", userRegistrationDTO.getPassword())) {

                if(userService.existUser(userRegistrationDTO.getEmail())) { //caso in cui il mio utente si sia già registrato
                    return Response.status(Response.Status.BAD_REQUEST).entity("User already exists").build();
                }
                
                userService.register(userRegistrationDTO);
                return Response.ok().build();
                
            } else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

    }

    @POST
    @Path("/login")
    public Response userLogin(@RequestBody UserLoginDTO userLoginDTO) {
        try {
           if(userService.login(userLoginDTO)) {
               return Response.ok(issueToken(userLoginDTO.getEmail())).build();
           }
           return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    public UserLoginResponseDTO issueToken(String email) {

        byte[] secret = "stringachiavesegreta12345123411111".getBytes();

        Key key = Keys.hmacShaKeyFor(secret);

        User user = userService.findByEmail(email);

        // Mappa dei claims
        Map<String, Object> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("lastname", user.getLastname());
        map.put("email", email);
        map.put("role", user.getRole());

        Date creationDate = new Date();
        Date endDate = Timestamp.valueOf(LocalDateTime.now().plusMinutes(15L));
        String JWTToken = Jwts.builder().setClaims(map)
                .setIssuer("http://localhost:8080")
                .setIssuedAt(creationDate)
                .setExpiration(endDate)
                .signWith(key)
                .compact();

        UserLoginResponseDTO userLoginResponseDTO = new UserLoginResponseDTO();
        userLoginResponseDTO.setToken(JWTToken);
        userLoginResponseDTO.setTokenCreationTime(creationDate);
        userLoginResponseDTO.setTtl(endDate);

        UserDTO userDTO = mapper.map(user, UserDTO.class);

        userLoginResponseDTO.setUser(userDTO);
        

        return userLoginResponseDTO;
    }

}
