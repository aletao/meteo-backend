package it.corso.service;

import it.corso.dto.UserLoginDTO;
import it.corso.dto.UserRegistrationDTO;
import it.corso.model.User;

public interface UserService {

    boolean existUser(String email);

    User findByEmail(String email);

    void register(UserRegistrationDTO userRegistrationDTO);

    boolean login(UserLoginDTO userLoginDTO);
    
}
