package it.corso.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dto.ForecastRequestDTO;
import it.corso.model.ForecastRequest;
import it.corso.model.User;
import it.corso.model.WeatherCondition;
import it.corso.repository.ForecastRequestRepository;
import it.corso.repository.WeatherConditionRepository;

@Service
public class ForecastRequestServiceImpl implements ForecastRequestService {

    @Autowired
    private UserService userService;

    @Autowired
    private ForecastRequestRepository forecastRequestRepository;

    @Autowired
    private WeatherConditionRepository weatherConditionRepository;

    private ModelMapper mapper = new ModelMapper();


    @Override
    public List<ForecastRequestDTO> getAllForecastRequestFromEmail(String email) {

        User user = userService.findByEmail(email);
        List<ForecastRequest> forecastRequests = forecastRequestRepository.findByUserId(user.getId());
        System.out.println(forecastRequests);

        return forecastRequests.stream().map(forecastRequest -> mapper.map(forecastRequest, ForecastRequestDTO.class)).collect(Collectors.toList());
    }


    @Override
    public ForecastRequestDTO createForecast(JSONObject responseJson, String email, String city) {

        // per prima cosa devo creare gli oggetti WeatherCondition e WeatherForecast
        // poi devo popolarli con i dati del JSONObject
        // poi devo salvarli nel database

        ForecastRequest forecastRequest = new ForecastRequest();
        forecastRequest.setRequestDate(new Date().toString());

        // creare la lista di WeatherCondition
        JSONObject daily = responseJson.getJSONObject("daily");
        Double latitude = responseJson.getDouble("latitude");
        Double longitude = responseJson.getDouble("longitude");
        JSONArray weatherCodes = daily.getJSONArray("weather_code");
        JSONArray maxTemperatures = daily.getJSONArray("temperature_2m_max");
        JSONArray minTemperatures = daily.getJSONArray("temperature_2m_min");
        JSONArray dates = daily.getJSONArray("time");

        // creo la lista di WeatherCondition con un ciclo for
        List<WeatherCondition> weatherConditions = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            WeatherCondition weatherCondition = new WeatherCondition();
            weatherCondition.setCity(city);
            weatherCondition.setLatitude(latitude);
            weatherCondition.setLongitude(longitude);
            weatherCondition.setWeatherCode(weatherCodes.getInt(i));
            weatherCondition.setWeatherStatus(translateWeatherCode(weatherCodes.getInt(i)));
            weatherCondition.setMaxTemperature(maxTemperatures.getDouble(i));
            weatherCondition.setMinTemperature(minTemperatures.getDouble(i));
            weatherCondition.setDate(dates.getString(i));
            weatherCondition.setForecastRequest(forecastRequest);
            weatherConditions.add(weatherCondition);
        }

        forecastRequest.setWeatherConditions(weatherConditions);

        User user = userService.findByEmail(email);

        forecastRequest.setUser(user);

        forecastRequestRepository.save(forecastRequest);

        weatherConditionRepository.saveAll(weatherConditions);

        ForecastRequestDTO forecastRequestDTO = mapper.map(forecastRequest, ForecastRequestDTO.class);

        return forecastRequestDTO;

    }

    @Override
    public ForecastRequestDTO searchForecast(JSONObject responseJson, String city) {

        /* Funzione per cercare senza salvare sul db */

        ForecastRequest forecastRequest = new ForecastRequest();
        forecastRequest.setRequestDate(new Date().toString());

        // creare la lista di WeatherCondition
        JSONObject daily = responseJson.getJSONObject("daily");
        Double latitude = responseJson.getDouble("latitude");
        Double longitude = responseJson.getDouble("longitude");
        JSONArray weatherCodes = daily.getJSONArray("weather_code");
        JSONArray maxTemperatures = daily.getJSONArray("temperature_2m_max");
        JSONArray minTemperatures = daily.getJSONArray("temperature_2m_min");
        JSONArray dates = daily.getJSONArray("time");

        // creo la lista di WeatherCondition con un ciclo for
        List<WeatherCondition> weatherConditions = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            WeatherCondition weatherCondition = new WeatherCondition();
            weatherCondition.setCity(city);
            weatherCondition.setLatitude(latitude);
            weatherCondition.setLongitude(longitude);
            weatherCondition.setWeatherCode(weatherCodes.getInt(i));
            weatherCondition.setWeatherStatus(translateWeatherCode(weatherCodes.getInt(i)));
            weatherCondition.setMaxTemperature(maxTemperatures.getDouble(i));
            weatherCondition.setMinTemperature(minTemperatures.getDouble(i));
            weatherCondition.setDate(dates.getString(i));
            weatherCondition.setForecastRequest(forecastRequest);
            weatherConditions.add(weatherCondition);
        }

        forecastRequest.setWeatherConditions(weatherConditions);

        ForecastRequestDTO forecastRequestDTO = mapper.map(forecastRequest, ForecastRequestDTO.class);

        return forecastRequestDTO;

    }
    

    @Override
    public Double[] getCoordinatesFromCityName(String city) {
        try {
            URI uri = new URI("https://geocoding-api.open-meteo.com/v1/search?name=" + city + "&count=1&language=en&format=json");
            HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                throw new Exception("Error");
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String inputLine;

            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            JSONObject responseJson = new JSONObject(response.toString());

            if (responseJson.has("results")) {
                JSONArray resultsArray = responseJson.getJSONArray("results");
                if (resultsArray.length() > 0) {
                    JSONObject firstResult = resultsArray.getJSONObject(0);
                    Double latitude = firstResult.getDouble("latitude");
                    Double longitude = firstResult.getDouble("longitude");
                    return new Double[]{latitude, longitude};
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String translateWeatherCode(int weatherCode) {
        switch (weatherCode) {
            case 0:
                return "Clear sky";
            case 1:
            case 2:
            case 3:
                return "Mainly clear, partly cloudy, and overcast";
            case 45:
            case 48:
                return "Fog and depositing rime fog";
            case 51:
            case 53:
            case 55:
                return "Drizzle: Light, moderate, and dense intensity";
            case 56:
            case 57:
                return "Freezing Drizzle: Light and dense intensity";
            case 61:
            case 63:
            case 65:
                return "Rain: Slight, moderate and heavy intensity";
            case 66:
            case 67:
                return "Freezing Rain: Light and heavy intensity";
            case 71:
            case 73:
            case 75:
                return "Snow fall: Slight, moderate, and heavy intensity";
            case 77:
                return "Snow grains";
            case 80:
            case 81:
            case 82:
                return "Rain showers: Slight, moderate, and violent";
            case 85:
            case 86:
                return "Snow showers slight and heavy";
            case 95:
                return "Thunderstorm: Slight or moderate";
            case 96:
            case 99:
                return "Thunderstorm with slight and heavy hail";
            default:
                return "Unknown";
        }
    }

}
