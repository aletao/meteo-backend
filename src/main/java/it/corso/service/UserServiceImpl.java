package it.corso.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dto.UserLoginDTO;
import it.corso.dto.UserRegistrationDTO;
import it.corso.model.User;
import it.corso.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private ModelMapper mapper = new ModelMapper();

    @Override
    public boolean existUser(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void register(UserRegistrationDTO userRegistrationDTO) {

        User user = mapper.map(userRegistrationDTO, User.class);
        user.setPassword(DigestUtils.sha256Hex(userRegistrationDTO.getPassword()));
        userRepository.save(user);
        
    }

    @Override
    public boolean login(UserLoginDTO userLoginDTO) {
        User user = mapper.map(userLoginDTO, User.class);

        user.setPassword(DigestUtils.sha256Hex(userLoginDTO.getPassword()));

        User foundUser = userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());

        return foundUser != null ? true : false;
    }

}
