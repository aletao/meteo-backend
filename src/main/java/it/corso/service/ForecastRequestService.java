package it.corso.service;

import java.util.List;

import org.json.JSONObject;

import it.corso.dto.ForecastRequestDTO;

public interface ForecastRequestService {

    ForecastRequestDTO createForecast(JSONObject responseJson, String email, String city);
    
    ForecastRequestDTO searchForecast(JSONObject responseJson, String city);
    
    String translateWeatherCode(int weatherCode);

    Double[] getCoordinatesFromCityName(String city);

    List<ForecastRequestDTO> getAllForecastRequestFromEmail(String email);

}