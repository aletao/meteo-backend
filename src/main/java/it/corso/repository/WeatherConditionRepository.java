package it.corso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.model.WeatherCondition;

public interface WeatherConditionRepository extends JpaRepository<WeatherCondition, Integer>{
    
}
