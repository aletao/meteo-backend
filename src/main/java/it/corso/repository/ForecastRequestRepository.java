package it.corso.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.model.ForecastRequest;

public interface ForecastRequestRepository extends JpaRepository<ForecastRequest, Integer>{

    List<ForecastRequest> findByUserId(Integer id);
    
}
