package it.corso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

    boolean existsByEmail(String email);

    User findByEmailAndPassword(String email, String password);

    User findByEmail(String email);
    
}
