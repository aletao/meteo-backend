package it.corso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeteoBackendApplication {
	// Alessandro Taormina
	public static void main(String[] args) {
		SpringApplication.run(MeteoBackendApplication.class, args);
	}

}
