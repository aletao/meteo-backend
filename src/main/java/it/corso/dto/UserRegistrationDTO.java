package it.corso.dto;

import jakarta.validation.constraints.Pattern;

public class UserRegistrationDTO {

    @Pattern(regexp = "[a-zA-Z]{1,50}", message = "Input not valid")
    private String name;

    @Pattern(regexp = "[a-zA-Z]{1,50}", message = "Input not valid")
    private String lastname;

    @Pattern(regexp = "[a-zA-Z0-9\\.\\+_-]+@[a-zA-Z0-9\\._-]+\\.[a-zA-Z]{2,8}", message = "Input not valid")
    private String email;

    private String password;


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
}
