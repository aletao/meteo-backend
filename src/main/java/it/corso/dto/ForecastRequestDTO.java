package it.corso.dto;

import java.util.List;

public class ForecastRequestDTO {

    private Integer id;
    private String requestDate;
    private List<WeatherConditionDTO> weatherConditions;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequestDate() {
        return this.requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public List<WeatherConditionDTO> getWeatherConditions() {
        return this.weatherConditions;
    }

    public void setWeatherConditions(List<WeatherConditionDTO> weatherConditions) {
        this.weatherConditions = weatherConditions;
    }

}
