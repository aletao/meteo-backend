package it.corso.dto;

public class UserDTO {

    private String name;

    private String lastname;

    private String email;
    
    //private String password;
    private RoleDTO role;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RoleDTO getRole() {
        return this.role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    
}
