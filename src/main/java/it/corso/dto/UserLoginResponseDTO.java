package it.corso.dto;

import java.util.Date;

public class UserLoginResponseDTO {

    private String token;

    private Date ttl;

    private Date tokenCreationTime;

    private UserDTO user;


    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTtl() {
        return this.ttl;
    }

    public void setTtl(Date ttl) {
        this.ttl = ttl;
    }

    public Date getTokenCreationTime() {
        return this.tokenCreationTime;
    }

    public void setTokenCreationTime(Date tokenCreationTime) {
        this.tokenCreationTime = tokenCreationTime;
    }

    public UserDTO getUser() {
        return this.user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    
}
